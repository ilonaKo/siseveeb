<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match forms.
    |
    */

    // CREATE NEW USER FORM
    'create_user_label_email'           => 'Kasutaja E-post',
    'create_user_ph_email'              => 'Kasutaja E-post',
    'create_user_icon_email'            => 'fa-envelope',

    'create_user_label_username'        => 'Kasutajanimi',
    'create_user_ph_username'           => 'Kasutajanimi',
    'create_user_icon_username'         => 'fa-user',

    'create_user_label_firstname'       => 'Eesnimi',
    'create_user_ph_firstname'          => 'Eesnimi',
    'create_user_icon_firstname'        => 'fa-user',

    'create_user_label_lastname'        => 'Perenimi',
    'create_user_ph_lastname'           => 'Perenimi',
    'create_user_icon_lastname'         => 'fa-user',

    'create_user_label_password'        => 'Parool',
    'create_user_ph_password'           => 'Parool',
    'create_user_icon_password'         => 'fa-lock',

    'create_user_label_pw_confirmation' => 'Kinnita parool',
    'create_user_ph_pw_confirmation'    => 'Kinnita parool',
    'create_user_icon_pw_confirmation'  => 'fa-lock',

    'create_user_label_location'        => 'Kasutaja asukoht',
    'create_user_ph_location'           => 'Kasutaja asukoht',
    'create_user_icon_location'         => 'fa-map-marker',

    'create_user_label_bio'             => 'Kasutaja elulugu',
    'create_user_ph_bio'                => 'Kasutaja elulugu',
    'create_user_icon_bio'              => 'fa-pencil',

    'create_user_label_twitter_username' => 'Kasutaja Twitteri nimi',
    'create_user_ph_twitter_username'    => 'Kasutaja Twitteri nimi',
    'create_user_icon_twitter_username'  => 'fa-twitter',

    'create_user_label_github_username' => 'Kasutaja GitHubi nimi',
    'create_user_ph_github_username'    => 'Kasutaja GitHubi nimi',
    'create_user_icon_github_username'  => 'fa-github',

    'create_user_label_career_title'    => 'Kasutaja amet',
    'create_user_ph_career_title'       => 'Kasutaja amet',
    'create_user_icon_career_title'     => 'fa-briefcase',

    'create_user_label_education'       => 'Kasutaja haridus',
    'create_user_ph_education'          => 'Kasutaja haridus',
    'create_user_icon_education'        => 'fa-graduation-cap',

    'create_user_label_role'            => 'Kasutaja roll',
    'create_user_ph_role'               => 'Vali kasutaja roll',
    'create_user_icon_role'             => 'fas fa-fw fas fa-shield-alt',

    'create_user_button_text'           => '<i class="fa fa-user-plus" aria-hidden="true"></i> Loo uus kasutaja',

    // EDIT USER AS ADMINISTRATOR FORM
    'edit-user-admin-title'             => 'Muuda kasutaja infot',

    'label-username'                    => 'Kasutajanimi',
    'ph-username'                       => 'Kasutajanimi',

    'label-useremail'                   => 'Kasutaja E-post',
    'ph-useremail'                      => 'Kasutaja E-post',

    'label-userrole_id'                 => 'Kasutaja tase',
    'option-label'                      => 'Vali tase',
    'option-user'                       => 'Kasutaja',
    'option-editor'                     => 'Muutja',
    'option-admin'                      => 'Administraator',
    'submit-btn-text'                   => 'Muuda kasutajat',

    'submit-btn-icon'                   => 'fa-save',
    'username-icon'                     => 'fa-user',
    'useremail-icon'                    => 'fa-envelope-o',

    'change-pw'                         => 'Muuda parool',
    'cancel'                            => 'Tühista',
    'save-changes'                      => '<i class="fa fa-fw fa-save" aria-hidden="true"></i> Salvesta muudatused',

    // Search Users Form
    'search-users-ph'                   => 'Otsi kasutajaid',

];
