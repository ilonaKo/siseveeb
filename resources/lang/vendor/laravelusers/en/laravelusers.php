<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel Users Blades Language Lines
    |--------------------------------------------------------------------------
    */

    'showing-all-users'     => 'Näitan kõiki kasutajaid',
    'users-menu-alt'        => 'Näita kasutajate haldamise menüüd',
    'create-new-user'       => 'Loo uus kasutaja',
    'show-deleted-users'    => 'Näita kustutatud kasutaja',
    'editing-user'          => 'Muudan kasutajat :name',
    'showing-user'          => 'Näitan kasutajat :name',
    'showing-user-title'    => ':name informatsioon',

    'users-table' => [
        'caption'   => '{1} :userscount kasutajat kokku|[2,*] :userscount total users',
        'id'        => 'ID',
        'name'      => 'Nimi',
        'email'     => 'E-post',
        'role'      => 'Roll',
        'created'   => 'Loodud',
        'updated'   => 'Uuendatud',
        'actions'   => 'Tegevused',
        'updated'   => 'Uuendatud',
    ],

    'buttons' => [
        'create-new'    => '<span class="hidden-xs hidden-sm">Uus kasutaja</span>',
        'delete'        => '<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Kustuta</span><span class="hidden-xs hidden-sm hidden-md"> kasutaja</span>',
        'show'          => '<i class="fas fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Vaata</span><span class="hidden-xs hidden-sm hidden-md"> kasutaja</span>',
        'edit'          => '<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Muuda</span><span class="hidden-xs hidden-sm hidden-md"> kasutaja</span>',
        'back-to-users' => '<span class="hidden-sm hidden-xs">Tagasi </span><span class="hidden-xs">kasutajad lehele</span>',
        'back-to-user'  => 'Tagasi  <span class="hidden-xs">kasutaja lehele</span>',
        'delete-user'   => '<i class="far fa-trash-alt fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Kustuta</span><span class="hidden-xs"> kasutaja</span>',
        'edit-user'     => '<i class="fas fa-pencil-alt fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Muuda</span><span class="hidden-xs"> kasutaja</span>',
    ],

    'tooltips' => [
        'delete'        => 'Kustuta',
        'show'          => 'Vaata',
        'edit'          => 'Muuda',
        'create-new'    => 'Loo uus kasutaja',
        'back-users'    => 'Tagasi kasutajad lehele',
        'email-user'    => 'E-post :user',
        'submit-search' => 'Otsi kasutajat',
        'clear-search'  => 'Tühjenda otsingu tulemus',
    ],

    'messages' => [
        'userNameTaken'          => 'Kasutajanimi on võetud',
        'userNameRequired'       => 'Kasutajanimi on puudu',
        'fNameRequired'          => 'Eesnimi on puudu',
        'lNameRequired'          => 'Perenimi on puudu',
        'emailRequired'          => 'E-post on puudu',
        'emailInvalid'           => 'E-post on vale',
        'passwordRequired'       => 'Parool on puudu',
        'PasswordMin'            => 'Parooli min pikkus on 8 märki',
        'PasswordMax'            => 'Parooli max pikkus on 20 märki',
        'captchaRequire'         => 'Captcha on puudu',
        'CaptchaWrong'           => 'Vale captcha, palun proovi uuesti.',
        'roleRequired'           => 'Kasutaja roll on puudu.',
        'user-creation-success'  => 'Kasutaja loodud',
        'update-user-success'    => 'Kasutaja uuendatud',
        'delete-success'         => 'kasutaja kustutatud',
        'cannot-delete-yourself' => 'Sa ei saa ennast kustutada',
    ],

    'show-user' => [
        'id'                => 'Kasutaja ID',
        'name'              => 'Kasutajanimi',
        'email'             => '<span class="hidden-xs">Kasutaja </span>E-post',
        'role'              => 'Kasutaja roll',
        'created'           => 'Loodud <span class="hidden-xs"></span>',
        'updated'           => 'Uuendatud <span class="hidden-xs"></span>',
        'labelRole'         => 'Kasutaja roll',
        'labelAccessLevel'  => '<span class="hidden-xs">Kasutaja</span> tase|<span class="hidden-xs">Kasutaja</span> Tasemed',
    ],

    'search'  => [
        'title'         => 'Näitan otsingu tulemusi',
        'found-footer'  => ' leitud',
        'no-results'    => 'Tulemit ei ole',
    ],
];
