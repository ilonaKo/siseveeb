<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.messages', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> <!-- vea, j.t teated -->
<div class="container">
    <a href="/home" class="btn btn-secondary">Tagasi</a>
    <div class="card">
        <div class="card-header">See on ürituse sisestamise leht</div>
            <div class="card-body">
            <?php echo Form::open(['action' => 'YritusedController@store', 'method' => 'POST']); ?>

                <div class="form-group row">
                    <?php echo e(Form::label('yksyks', '1.1. Pühapäevane või pühade jumalateenistus', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='yksyks'>
                    <input type='checkbox' id='test' value='Ei' name='yksyks'>
                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('ykskaks', '1.2. Jumalateenistusest võttis osa (mitu inimest?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('ykskaks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('ykskolm', '1.3. Armulauaga jumalateenistus', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='ykskolm'>
                    <input type='checkbox' id='test' value='Ei' name='ykskolm'>
                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('yksneli', '1.4. Armulauale võeti (mitu inimest?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('yksneli', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('yksviis', '1.5. Kontsert oli ', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='yksviis'>
                    <input type='checkbox' id='test' value='Ei' name='yksviis'>
                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('ykskuus', '1.6. Teenistus oli ', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='ykskuus'>
                    <input type='checkbox' id='test' value='Ei' name='ykskuus'>
                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('ykskuusyks', '1.6.1. korraline abiline (abiõp, diakon, praktikant, jutlustaja) (mitu korda?)', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='ykskuusyks'>
                    <input type='checkbox' id='test' value='Ei' name='ykskuusyks'>
                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('ykskuuskaks', '1.6.2. keegi teine', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='ykskuuskaks'>
                    <input type='checkbox' id='test' value='Ei' name='ykskuuskaks'>
                </div>
                <hr>
                <div class="form-group row">
                    <?php echo e(Form::label('kaksyks', '2.1. Ristiti (mitu inimest?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kaksyks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kakskaks', '2.2. Leeritati (mitu inimest?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kakskaks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kakskolm', '2.3. Laulatati (mitu paari?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kakskolm', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kaksneli', '2.4. Abielu registreeriti (mitmel korral?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kaksneli', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kaksviis', '2.5. Maeti (mitu inimest?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kaksviis', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kakskuus', '2.6. Kodust armulauda jagati (mitmele inimesele?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kakskuus', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kaksseitse', '2.7. Muid talitusi peeti (pühitsemised, õnnistamised, kihlused) ', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='kaksseitse'>
                    <input type='checkbox' id='test' value='Ei' name='kaksseitse'>
                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kakskaheksa', '2.8. Kodusid külastati ', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='kakskaheksa'>
                    <input type='checkbox' id='test' value='Ei' name='kakskaheksa'>
                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kaksyheksa', '2.9. Palvusi vanglas, haiglas, hoolekandeasutuses, jms ', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='kaksyheksa'>
                    <input type='checkbox' id='test' value='Ei' name='kaksyheksa'>
                </div>
                <hr>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmyksyks', '3.1.1. Lastetöös osales (mitu last?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmyksyks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmykskaks', '3.1.2. Lastetunde peeti (tunniks arvestatakse üks akadeemiline tund = 45 minutit) (mitu tundi?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmykskaks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmykskolm', '3.1.3 Laste- ja pühapäevakoolitööd juhendas (mitu õpetajat?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmykskolm', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmyksneli', '3.1.4. Lastelaagri(te)s osales (laagriks loetakse vähemalt kahepäevast) (mitmu last?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmyksneli', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmkaksyks', '3.2.1 Noortetöös osales (mitu noort?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmkaksyks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmkakskaks', '3.2.2. Noortetööd juhendas (mitu juhendajat?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmkakskaks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmkakskolm', '3.2.3. Noortetunde peeti (tunniks arvestatakse üks akadeemiline tund = 45 minutit) (mitu tundi?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmkakskolm', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmkolmyks', '3.3.1. Leeriõpetust anti (tunniks arvestatakse üks akadeemiline tund = 45 minutit) (mitu tundi?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmkolmyks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmkolmkaks', '3.3.2. Leerilaagri(te)s osales (laagriks loetakse vähemalt kahepäevast) (mitu inimest?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmkolmkaks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmneliyks', '3.4.1. Muusikatöös osales (koguduse koorilauljate, solistide või pillimängijatena töös osalenute arv) (mitu inimest?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmneliyks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmnelikaks', '3.4.2. Muusikatööd juhendas (organistid, koorijuhid, organist-koorijuhid, ansamblijuhid) (mitu muusikut?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmnelikaks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmnelikolm', '3.4.3. Muusikakollektiive oli (muusikakollektiivid on koorid, ansamblid ja orkestrid) (mitu kollektiivi?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmnelikolm', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmnelineli', '3.4.4. Kooriharjutusi peeti (tunniks arvestatakse üks akadeemiline tund = 45 minutit) (mitu tundi?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmnelineli', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('kolmviis', '3.5. Kogudusetöös osales vabatahtlikena (mitu inimest?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('kolmviis', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <hr>
                <div class="form-group row">
                    <?php echo e(Form::label('neliyks', '4.1. Nõukogu koosolekuid peeti ', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='neliyks'>
                    <input type='checkbox' id='test' value='Ei' name='neliyks'>
                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('nelikaks', '4.2. Täiskogu koosolekuid nõukogu ülesannetes peeti ', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='nelikaks'>
                    <input type='checkbox' id='test' value='Ei' name='nelikaks'>
                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('nelikolm', '4.3. Juhatuse koosolekuid peeti ', ['class' => 'col-sm-5'])); ?>

                    <input type='hidden' id='testHidden' value='Jah' name='nelikolm'>
                    <input type='checkbox' id='test' value='Ei' name='nelikolm'>
                </div>
                <hr>
                <div class="form-group row">
                    <?php echo e(Form::label('viisyks', '5.1. Liikmeid kokku (kõik kogudusse ristimise läbi vastuvõetud ning teistest EELK kogudustest üle tulnud liikmed) (mitu inimest?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('viisyks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('viiskaks', '5.2. Annetajaliikmeid oli kokku (liikmemaksu või -annetuse tasunuid) (mitu inimest?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('viiskaks', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>
                <div class="form-group row">
                    <?php echo e(Form::label('viiskolm', '5.3. Täiskogu liikmeid oli kokku (koguduse konfirmeeritud liikmed, kes on aruandeaastal käinud vähemalt üks kord armulaual ja tasunud liikmemaksu (-annetuse)) (mitu inimest?)', ['class' => 'col-sm-5'])); ?>

                    <?php echo e(Form::number('viiskolm', null, ['class' => 'form-control col-sm-1', 'placeholder' => '0'])); ?>

                </div>

                <?php echo e(Form::submit('Esita', ['class' => 'btn btn-primary'])); ?><!-- nupp -->
            <?php echo Form::close(); ?>

            </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* C:\xampp\htdocs\siseveeb\resources\views/yritused/create.blade.php */ ?>