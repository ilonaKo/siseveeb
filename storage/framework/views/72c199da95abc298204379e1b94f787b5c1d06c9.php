<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.messages', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> <!-- vea, j.t teated -->
<div class="container">
    <a href="/home" class="btn btn-secondary">Tagasi</a>
    <div class="card">
        <div class="card-header">Üritused</div>
            <div class="card-body">
                <?php if(count($yritused) > 0): ?>
                    <?php $__currentLoopData = $yritused; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $yritus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <h3><a href="/yritused/<?php echo e($yritus->id); ?>">Ürituse ID on: <?php echo e($yritus->id); ?></a></h3>
                        <small>Loodud <?php echo e($yritus->created_at); ?></small>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php echo e($yritused->links()); ?> <!-- lk numbrite jaoks -->
                <?php else: ?>
                    <p>Üritusi ei ole</p>
                <?php endif; ?>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* C:\xampp\htdocs\siseveeb\resources\views/yritused/index.blade.php */ ?>