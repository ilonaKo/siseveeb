<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.messages', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> <!-- vea, j.t teated -->
<div class="container">
<a href="/yritused" class="btn btn-secondary">Tagasi ürituste lehele</a>
    <div class="card">
        <div class="card-header">Ürituse ID on: <?php echo e($yritus->id); ?></div>
        <div class="card-body">
                <p>1.1. Pühapäevane või pühade jumalateenistus: <b><?php echo $yritus->yksyks; ?></b></p> <!-- näitamaks html -i -->
                <p>1.2. Jumalateenistusest võttis osa: <b><?php echo $yritus->ykskaks; ?> inimest</b></p>
                <p>1.3. Armulauaga jumalateenistus: <b><?php echo $yritus->ykskolm; ?></b></p>
                <p>1.4. Armulauale võeti: <b><?php echo $yritus->yksneli; ?> inimest</b></p>
                <p>1.5. Toimus kontsert: <b><?php echo $yritus->yksviis; ?></b></p>
                <p>1.6. Tegu oli teenistusega: <b><?php echo $yritus->ykskuus; ?></b></p>
                <p>1.6.1. osales korraline abiline (abiõp, diakon, praktikant, jutlustaja)?: <b><?php echo $yritus->ykskuusyks; ?></b></p>
                <p>1.6.2. keegi teine?: <b><?php echo $yritus->ykskuuskaks; ?></b></p>
                <hr>
                <p>2.1. Ristiti: <b><?php echo $yritus->kaksyks; ?> inimest </b></p>
                <p>2.2. Leeritati: <b><?php echo $yritus->kakskaks; ?> inimest </b></p>
                <p>2.3. Laulatati: <b><?php echo $yritus->kakskolm; ?> paari </b></p>
                <p>2.4. Abielu registreeriti: <b><?php echo $yritus->kaksneli; ?> korral</b></p>
                <p>2.5. Maeti: <b><?php echo $yritus->kaksviis; ?> inimest </b></p>
                <p>2.6. Kodust armulauda jagati: <b><?php echo $yritus->kakskuus; ?> inimesele</b></p>
                <p>2.7. Muid talitusi peeti (pühitsemised, õnnistamised, kihlused): <b><?php echo $yritus->kaksseitse; ?></b></p>
                <p>2.8. Kodusid külastati: <b><?php echo $yritus->kakskaheksa; ?></b></p>
                <p>2.9. Palvusi vanglas, haiglas, hoolekandeasutuses, jms.: <b><?php echo $yritus->kaksyheksa; ?></b></p>
                <hr>
                <p>3.1.1. Lastetöös osales: <b><?php echo $yritus->kolmyksyks; ?> last</b></p>
                <p>3.1.2. Lastetunde peeti (tunniks arvestatakse üks akadeemiline tund = 45 minutit): <b><?php echo $yritus->kolmykskaks; ?> tundi</b></p>
                <p>3.1.3 Laste- ja pühapäevakoolitööd juhendas: <b><?php echo $yritus->kolmykskolm; ?> õpetajat</b></p>
                <p>3.1.4. Lastelaagri(te)s osales (laagriks loetakse vähemalt kahepäevast): <b><?php echo $yritus->kolmyksneli; ?> last</b></p>
                <p>3.2.1 Noortetöös osales: <b><?php echo $yritus->kolmkaksyks; ?></b> noort</p>
                <p>3.2.2. Noortetööd juhendas: <b><?php echo $yritus->kolmkakskaks; ?></b> juhendajat</p>
                <p>3.2.3. Noortetunde peeti (tunniks arvestatakse üks akadeemiline tund = 45 minutit): <b><?php echo $yritus->kolmkakskolm; ?> tundi</b></p>
                <p>3.3.1. Leeriõpetust anti (tunniks arvestatakse üks akadeemiline tund = 45 minutit): <b><?php echo $yritus->kolmkolmyks; ?> tundi</b></p>
                <p>3.3.2. Leerilaagri(te)s osales (laagriks loetakse vähemalt kahepäevast): <b><?php echo $yritus->kolmkolmkaks; ?> inimest</b></p>
                <p>3.4.1. Muusikatöös osales (koguduse koorilauljate, solistide või pillimängijatena töös osalenute arv): <b><?php echo $yritus->kolmneliyks; ?> inimest</b></p>
                <p>3.4.2. Muusikatööd juhendas (organistid, koorijuhid, organist-koorijuhid, ansamblijuhid): <b><?php echo $yritus->kolmnelikaks; ?> muusikut</b></p>
                <p>3.4.3. Muusikakollektiive oli (muusikakollektiivid on koorid, ansamblid ja orkestrid): <b><?php echo $yritus->kolmnelikolm; ?> kollektiivi</b></p>
                <p>3.4.4. Kooriharjutusi peeti (tunniks arvestatakse üks akadeemiline tund = 45 minutit): <b><?php echo $yritus->kolmnelineli; ?> tundi</b></p>
                <p>3.5. Kogudusetöös osales vabatahtlikena: <b><?php echo $yritus->kolmviis; ?> inimest</b></p>
                <hr>
                <p>4.1. Nõukogu koosolekuid peeti: <b><?php echo $yritus->neliyks; ?></b></p>
                <p>4.2. Täiskogu koosolekuid nõukogu ülesannetes peeti: <b><?php echo $yritus->nelikaks; ?></b></p>
                <p>4.3. Juhatuse koosolekuid peeti: <b><?php echo $yritus->nelikolm; ?></b></p>
                <hr>
                <p>5.1. Liikmeid kokku (kõik kogudusse ristimise läbi vastuvõetud ning teistest EELK kogudustest üle tulnud liikmed): <b><?php echo $yritus->viisyks; ?> inimest</b></p>
                <p>5.2. Annetajaliikmeid oli kokku (liikmemaksu või -annetuse tasunuid): <b><?php echo $yritus->viiskaks; ?> inimest</b></p>
                <p>5.3. Täiskogu liikmeid oli kokku (koguduse konfirmeeritud liikmed, kes on aruandeaastal käinud vähemalt üks kord armulaual ja tasunud liikmemaksu (-annetuse)): <b><?php echo $yritus->viiskolm; ?> inimest</b></p>

            <hr>
            <small>Loodud <?php echo e($yritus->created_at); ?></small>
            <hr>
            <?php if(!Auth::guest()): ?> <!-- näed ainult siis kui sa ei ole guest -->
                <a href="/siseveeb/yritused/<?php echo e($yritus->id); ?>/edit" class="btn btn-secondary">Muuda</a>
                    <?php echo Form::open(['action' => ['YritusedController@destroy', $yritus->id], 'method' => 'POST', 'class' => 'float-right']); ?>

                        <?php echo e(Form::hidden('_method', 'DELETE')); ?>

                        <?php echo e(Form::submit('Kustuta', ['class' => 'btn btn-danger'])); ?>

                    <?php echo Form::close(); ?>

            <?php endif; ?>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* C:\xampp\htdocs\siseveeb\resources\views/yritused/show.blade.php */ ?>