<?php $__env->startSection('content'); ?>
<div class="container">
    <a href="/siseveeb/home" class="btn btn-secondary">Tagasi</a>
    <div class="card">
            <div class="card-header">Statistika on keritav tööriistariba abil vasakule ja paremale</div>
            <div class="card-body">
            <table class="table table-bordered table-sm table-responsive">
            <tr>
                <th>ID</th>
                <th>1.1.</th>
                <th>1.2.</th>
                <th>1.3.</th>
                <th>1.4.</th>
                <th>1.5.</th>
                <th>1.6.</th>
                <th>1.6.1.</th>
                <th>1.6.2.</th>
                <th>1.7.</th>
                <th>2.1.</th>
                <th>2.2.</th>
                <th>2.3.</th>
                <th>2.4.</th>
                <th>2.5.</th>
                <th>2.6.</th>
                <th>2.7.</th>
                <th>2.8.</th>
                <th>2.9.</th>
                <th>3.1.1.</th>
                <th>3.1.2.</th>
                <th>3.1.3.</th>
                <th>3.1.4.</th>
                <th>3.2.1.</th>
                <th>3.2.2.</th>
                <th>3.2.3.</th>
                <th>3.3.1.</th>
                <th>3.3.2.</th>
                <th>3.4.1.</th>
                <th>3.4.2.</th>
                <th>3.4.3.</th>
                <th>3.4.4.</th>
                <th>3.5.</th>
                <th>4.1.</th>
                <th>4.2.</th>
                <th>4.3.</th>
                <th>5.1.</th>
                <th>5.2.</th>
                <th>5.3.</th>
            </tr>
            <?php $__currentLoopData = $kokku; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $roww): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                <td><?php echo e($roww->id); ?></td>
                <td><?php echo e($roww->yksyks); ?></td>
                <td><?php echo e($roww->ykskaks); ?></td>
                <td><?php echo e($roww->ykskolm); ?></td>
                <td><?php echo e($roww->yksneli); ?></td>
                <td><?php echo e($roww->yksviis); ?></td>
                <td><?php echo e($roww->ykskuus); ?></td>
                <td><?php echo e($roww->ykskuusyks); ?></td>
                <td><?php echo e($roww->ykskuuskaks); ?></td>
                <td><?php echo e($roww->kaksyks); ?></td>
                <td><?php echo e($roww->kakskaks); ?></td>
                <td><?php echo e($roww->kakskolm); ?></td>
                <td><?php echo e($roww->kaksneli); ?></td>
                <td><?php echo e($roww->kaksviis); ?></td>
                <td><?php echo e($roww->kakskuus); ?></td>
                <td><?php echo e($roww->kaksseitse); ?></td>
                <td><?php echo e($roww->kakskaheksa); ?></td>
                <td><?php echo e($roww->kaksyheksa); ?></td>
                <td><?php echo e($roww->kolmyksyks); ?></td>
                <td><?php echo e($roww->kolmykskaks); ?></td>
                <td><?php echo e($roww->kolmykskolm); ?></td>
                <td><?php echo e($roww->kolmyksneli); ?></td>
                <td><?php echo e($roww->kolmkaksyks); ?></td>
                <td><?php echo e($roww->kolmkakskaks); ?></td>
                <td><?php echo e($roww->kolmkakskolm); ?></td>
                <td><?php echo e($roww->kolmkolmyks); ?></td>
                <td><?php echo e($roww->kolmkolmkaks); ?></td>
                <td><?php echo e($roww->kolmneliyks); ?></td>
                <td><?php echo e($roww->kolmnelikaks); ?></td>
                <td><?php echo e($roww->kolmnelikolm); ?></td>
                <td><?php echo e($roww->kolmnelineli); ?></td>
                <td><?php echo e($roww->kolmviis); ?></td>
                <td><?php echo e($roww->neliyks); ?></td>
                <td><?php echo e($roww->nelikaks); ?></td>
                <td><?php echo e($roww->nelikolm); ?></td>
                <td><?php echo e($roww->viisyks); ?></td>
                <td><?php echo e($roww->viiskaks); ?></td>
                <td><?php echo e($roww->viiskolm); ?></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </table>

            <table class="table table-bordered table-sm table-responsive">
            <tr>
                <th>Arvutus</th>
                <th>1.1.</th>
                <th>1.2.</th>
                <th>1.3.</th>
                <th>1.4.</th>
                <th>1.5.</th>
                <th>1.6.</th>
                <th>1.6.1.</th>
                <th>1.6.2.</th>
                <th>1.7.</th>
                <th>2.1.</th>
                <th>2.2.</th>
                <th>2.3.</th>
                <th>2.4.</th>
                <th>2.5.</th>
                <th>2.6.</th>
                <th>2.7.</th>
                <th>2.8.</th>
                <th>2.9.</th>
                <th>3.1.1.</th>
                <th>3.1.2.</th>
                <th>3.1.3.</th>
                <th>3.1.4.</th>
                <th>3.2.1.</th>
                <th>3.2.2.</th>
                <th>3.2.3.</th>
                <th>3.3.1.</th>
                <th>3.3.2.</th>
                <th>3.4.1.</th>
                <th>3.4.2.</th>
                <th>3.4.3.</th>
                <th>3.4.4.</th>
                <th>3.5.</th>
                <th>4.1.</th>
                <th>4.2.</th>
                <th>4.3.</th>
                <th>5.1.</th>
                <th>5.2.</th>
                <th>5.3.</th>
            </tr>
            <tr>
                <td>summa</td>
                <td><?php echo e($sumyksyks); ?></td>
                <td><?php echo e($sumykskaks); ?></td>
                <td><?php echo e($sumykskolm); ?></td>
                <td><?php echo e($sumyksneli); ?></td>
                <td><?php echo e($sumyksviis); ?></td>
                <td><?php echo e($sumykskuus); ?></td>
                <td><?php echo e($sumykskuusyks); ?></td>
                <td><?php echo e($sumykskuuskaks); ?></td>

                <td><?php echo e($sumkaksyks); ?></td>
                <td><?php echo e($sumkakskaks); ?></td>
                <td><?php echo e($sumkakskolm); ?></td>
                <td><?php echo e($sumkaksneli); ?></td>
                <td><?php echo e($sumkaksviis); ?></td>
                <td><?php echo e($sumkakskuus); ?></td>
                <td><?php echo e($sumkaksseitse); ?></td>
                <td><?php echo e($sumkakskaheksa); ?></td>
                <td><?php echo e($sumkaksyheksa); ?></td>

                <td><?php echo e($sumkolmyksyks); ?></td>
                <td><?php echo e($sumkolmykskaks); ?></td>
                <td><?php echo e($sumkolmykskolm); ?></td>
                <td><?php echo e($sumkolmyksneli); ?></td>
                <td><?php echo e($sumkolmkaksyks); ?></td>
                <td><?php echo e($sumkolmkakskaks); ?></td>
                <td><?php echo e($sumkolmkakskolm); ?></td>
                <td><?php echo e($sumkolmkolmyks); ?></td>
                <td><?php echo e($sumkolmkolmkaks); ?></td>
                <td><?php echo e($sumkolmneliyks); ?></td>
                <td><?php echo e($sumkolmnelikaks); ?></td>
                <td><?php echo e($sumkolmnelikolm); ?></td>
                <td><?php echo e($sumkolmnelineli); ?></td>
                <td><?php echo e($sumkolmviis); ?></td>

                <td><?php echo e($sumneliyks); ?></td>
                <td><?php echo e($sumnelikaks); ?></td>
                <td><?php echo e($sumnelikolm); ?></td>
                <td><?php echo e($sumviisyks); ?></td>
                <td><?php echo e($sumviiskaks); ?></td>
                <td><?php echo e($sumviiskolm); ?></td>
            </tr>
            <tr>
                <td>keskmine</td>
                <td><?php echo e($avgyksyks); ?></td>
                <td><?php echo e($avgykskaks); ?></td>
                <td><?php echo e($avgykskolm); ?></td>
                <td><?php echo e($avgyksneli); ?></td>
                <td><?php echo e($avgyksviis); ?></td>
                <td><?php echo e($avgykskuus); ?></td>
                <td><?php echo e($avgykskuusyks); ?></td>
                <td><?php echo e($avgykskuuskaks); ?></td>

                <td><?php echo e($avgkaksyks); ?></td>
                <td><?php echo e($avgkakskaks); ?></td>
                <td><?php echo e($avgkakskolm); ?></td>
                <td><?php echo e($avgkaksneli); ?></td>
                <td><?php echo e($avgkaksviis); ?></td>
                <td><?php echo e($avgkakskuus); ?></td>
                <td><?php echo e($avgkaksseitse); ?></td>
                <td><?php echo e($avgkakskaheksa); ?></td>
                <td><?php echo e($avgkaksyheksa); ?></td>

                <td><?php echo e($avgkolmyksyks); ?></td>
                <td><?php echo e($avgkolmykskaks); ?></td>
                <td><?php echo e($avgkolmykskolm); ?></td>
                <td><?php echo e($avgkolmyksneli); ?></td>
                <td><?php echo e($avgkolmkaksyks); ?></td>
                <td><?php echo e($avgkolmkakskaks); ?></td>
                <td><?php echo e($avgkolmkakskolm); ?></td>
                <td><?php echo e($avgkolmkolmyks); ?></td>
                <td><?php echo e($avgkolmkolmkaks); ?></td>
                <td><?php echo e($avgkolmneliyks); ?></td>
                <td><?php echo e($avgkolmnelikaks); ?></td>
                <td><?php echo e($avgkolmnelikolm); ?></td>
                <td><?php echo e($avgkolmnelineli); ?></td>
                <td><?php echo e($avgkolmviis); ?></td>

                <td><?php echo e($avgneliyks); ?></td>
                <td><?php echo e($avgnelikaks); ?></td>
                <td><?php echo e($avgnelikolm); ?></td>
                <td><?php echo e($avgviisyks); ?></td>
                <td><?php echo e($avgviiskaks); ?></td>
                <td><?php echo e($avgviiskolm); ?></td>
            </tr>
            </table>
        </div>
        
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* C:\xampp\htdocs\siseveeb\resources\views/statistikad/index.blade.php */ ?>