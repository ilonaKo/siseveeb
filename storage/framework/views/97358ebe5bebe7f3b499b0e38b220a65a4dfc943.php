<?php $__env->startSection('content'); ?>
<!-- avalehe sisu -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Oled sisse logitud kasutajaga: <b><?php echo e(Auth::user()->name); ?></b></div>
                        <div class="card-body"> 
                           <div class="btns-container">
                            <?php if(session('status')): ?>
                                <div class="alert alert-success" role="alert">
                                    <?php echo e(session('status')); ?>

                                </div>
                            <?php endif; ?>
                            <p><a href="/yritused/create" class="btn-siseveeb">Kantsliklade</a></p>
                            <p><a href="/yritused" class="btn-siseveeb">Vaata üritusi</a></p>
                            <p><a href="https://drive.google.com/open?id=19oPMVKmX3pL7OW99ZgQLH6rL37HWtZzI" class="btn-siseveeb">Failide Drive</a></p>
                            <p><a href="/statistikad" class="btn-siseveeb">Vaata statistikat</a></p>
                            <p><a href="<?php echo e(route('users')); ?>" class="btn-siseveeb">Kasutajate haldamine</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $__env->stopSection(); ?>
    </body>
</html>

<?php echo $__env->make('layouts.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* C:\xampp\htdocs\siseveeb\resources\views/home.blade.php */ ?>