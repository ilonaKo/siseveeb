<!-- see on navbar peale sisse logimist -->
<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <title><?php echo e(config('app.name', 'Laravel')); ?></title>

        
        <?php if(config('laravelusers.enableBootstrapCssCdn')): ?>
            <link rel="stylesheet" type="text/css" href="<?php echo e(config('laravelusers.bootstrapCssCdn')); ?>">
        <?php endif; ?>
        <?php if(config('laravelusers.enableAppCss')): ?>
            <link rel="stylesheet" type="text/css" href="<?php echo e(asset(config('laravelusers.appCssPublicFile'))); ?>">
        <?php endif; ?>

        <?php echo $__env->yieldContent('template_linked_css'); ?>
        <link href="<?php echo e(asset('css/custom.css')); ?>" rel="stylesheet">

        
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>;
        </script>
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo e(url('/home')); ?>">
                        <?php echo e(config('app.name', 'Laravel')); ?>

                    </a>
                    
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            <?php if(auth()->guard()->guest()): ?>
                                <li><a class="nav-link" href="<?php echo e(route('login')); ?>">Logi sisse</a></li>
                                <li><a class="nav-link" href="<?php echo e(route('register')); ?>">Registreeri</a></li>
                            <?php else: ?>
                                <li class="nav-item">  
                                    <a class="nav-link" href="<?php echo e(route('logout')); ?>"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <?php echo e(__('Logi välja')); ?>

                                    </a>
          
                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>
                                </li>
                            <?php endif; ?>
                        </ul>                    
                </div>
            </nav>

            <main class="py-4">
                <?php echo $__env->yieldContent('content'); ?>
            </main>
        </div>


    </body>
</html>

<?php /* C:\xampp\htdocs\siseveeb\resources\views/layouts/navbar.blade.php */ ?>